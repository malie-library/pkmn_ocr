import tkinter as tk
from tkinter import ttk
from tkinter import font

import PIL.Image
import PIL.ImageTk


def prompt_user(confidence, rawtext, guesstext, image_file) -> str:
    rettext = ''

    root = tk.Tk()
    root.title("Low confidence transcribing text")
    default_font = font.nametofont("TkDefaultFont")
    default_font.configure(size=20)

    # "Help!"
    frm = ttk.Frame(root, padding=10)
    frm.grid()
    label = ttk.Label(
        frm,
        text=f"Low confidence ({confidence*100:.2f}%) reading text:",
        background="red"
    )
    label.grid(column=0, columnspan=2, row=0)

    # Image
    tkimg = PIL.ImageTk.PhotoImage(file=image_file)
    thingie = ttk.Label(frm, image=tkimg)
    thingie.grid(column=0, columnspan=2, row=1)

    # "Read: ..."
    label = ttk.Label(frm, text="Read:")
    label.grid(column=0, row=2, sticky="NW")
    label = ttk.Label(frm, text=rawtext)
    label.grid(column=1, row=2, sticky="NW")

    # "Guessed: ..."
    label = ttk.Label(frm, text="Guessed:")
    label.grid(column=0, row=3, sticky="NW")
    label = ttk.Label(frm, text=guesstext)
    label.grid(column=1, row=3, sticky="NW")

    # Prompt & Text
    label = ttk.Label(frm, text="Please enter the correct transcription:")
    label.grid(column=0, row=4, sticky="NW")

    text = tk.Text(frm, font=default_font, height=3)
    text.grid(column=0, columnspan=2, row=5, sticky="EW")
    text.insert("0.0", guesstext)

    #
    def enter_key(event):
        nonlocal rettext
        value = text.get("0.0", tk.END)
        rettext = value.strip()
        root.destroy()
        return "break"

    text.bind('<Return>', enter_key)
    text.focus_set()
    root.mainloop()
    return rettext
