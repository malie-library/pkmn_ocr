import csv
import json

entries = []

# Veekun flavor text entries
with open("pokemon_species_flavor_text.csv", "r") as infile:
    for entry in csv.DictReader(infile):
        if entry['language_id'] == '9':
            lines = entry['flavor_text'].replace('\x0c', '\n').split('\n')
            tmp = " ".join(lines)
            assert tmp
            entries.append(tmp)

# USUM flavor text for Alolan forms
with open('usum_form_flavor_addendums_en.json', 'r') as infile:
    data = json.load(infile)
    entries.extend(data)

# BDSP, PLA, SWSH and others.
with open("zukan.json", "r") as infile:
    zukan = json.load(infile)
    for entry in zukan['en-US'].values():
        assert entry
        lines = entry.replace('\\n', '\n').split('\n')
        entry = " ".join(lines)
        entries.append(entry)

# Scarlet + Violet
with open('svflavor_hackjob.txt', 'r') as infile:
    for line in infile:
        entries.append(line.strip())


entries = sorted(list(set(entries)))
with open("pkmn_flavor_en.json", "w") as outfile:
    json.dump(entries, outfile, ensure_ascii=False, indent=2)
